module gitlab.com/elixxir/primitives

go 1.13

require (
	github.com/badoux/checkmail v1.2.1
	github.com/nyaruka/phonenumbers v1.0.60
	github.com/pkg/errors v0.9.1
	github.com/spf13/jwalterweatherman v1.1.0
	github.com/stretchr/testify v1.6.1 // indirect
	gitlab.com/xx_network/primitives v0.0.4-0.20210121203635-8a771fc14f8a
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
